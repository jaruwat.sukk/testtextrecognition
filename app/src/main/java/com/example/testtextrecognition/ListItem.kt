package com.example.testtextrecognition


data class ListItem(
    var id: Int = 0,
    var dateTime: String = "",
    var url: String = ""
)