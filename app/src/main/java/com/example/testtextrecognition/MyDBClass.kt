package com.example.testtextrecognition

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper


class MyDBClass(context: Context) : SQLiteOpenHelper(context, DB_NAME, null, DB_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(
            "CREATE TABLE $TABLE_NAME " +
                    "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                    " $DATE_TIME TEXT," +
                    " $URL TEXT);"
        )
    }

    // Insert Data
    fun InsertData(dateTime: String, url: String): Long {
        try {
            val db: SQLiteDatabase
            db = this.writableDatabase // Write Data
            val Val = ContentValues()
            Val.put(DATE_TIME, dateTime)
            Val.put(URL, url)

            val rows = db.insert(TABLE_NAME, null, Val)

            db.close()
            return rows // return rows inserted.

        } catch (e: Exception) {
            return -1
        }

    }

    fun SelectAllData(): List<ListItem>? {
        // TODO Auto-generated method stub

        try {
            val MemberList = ArrayList<ListItem>()

            val db: SQLiteDatabase
            db = this.readableDatabase // Read Data

            val strSQL = "SELECT  * FROM $TABLE_NAME"
            val cursor = db.rawQuery(strSQL, null)

            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    do {
                        val item = ListItem()
                        item.id = cursor.getInt(0)
                        item.dateTime = cursor.getString(1)
                        item.url = cursor.getString(2)
                        MemberList.add(item)
                    } while (cursor.moveToNext())
                }
            }
            cursor!!.close()
            db.close()
            return MemberList

        } catch (e: Exception) {
            return null
        }

    }

    // Delete Data
    fun DeleteData(listId: String): Long {
        // TODO Auto-generated method stub

        try {

            val db: SQLiteDatabase
            db = this.writableDatabase // Write Data

            val rows = db.delete(
                TABLE_NAME, "_id = ?",
                arrayOf(listId)
            ).toLong()

            db.close()
            return rows // return rows delete.

        } catch (e: Exception) {
            return -1
        }

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }

    companion object {
        private val DB_NAME = "mydatabase"
        private val DB_VERSION = 1
        val TABLE_NAME = "Food"
        val DATE_TIME = "date_time"
        val URL = "url"
    }
}