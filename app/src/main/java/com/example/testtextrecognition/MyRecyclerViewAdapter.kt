package com.example.testtextrecognition

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.testtextrecognition.databinding.ListItemBinding

class MyRecyclerViewAdapter :
    RecyclerView.Adapter<MyRecyclerViewAdapter.ListItemViewHolder>() {

    var onUrlSelected: (url: String) -> Unit = {}
    var onClickDeleteImage: (id: String) -> Unit = {}
    var listItem = mutableListOf<ListItem>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListItemViewHolder {
        val view = DataBindingUtil.inflate<ListItemBinding>(
            LayoutInflater.from(parent.context),
            R.layout.list_item,
            parent,
            false
        )
        return ListItemViewHolder(view)
    }

    override fun getItemCount(): Int = listItem.size

    fun updateSurveyStatus(newSurveyStatusList: List<ListItem>) {
        listItem.clear()
        listItem.addAll(newSurveyStatusList)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ListItemViewHolder, position: Int) {
        holder.apply {
            bind(position)
            deleteItem()
            onUrlSelected()
        }
    }

    inner class ListItemViewHolder(private val binding: ListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            val item = listItem[position]
            binding.apply {
                listItem = item
                labelTextView1.text = item.id.toString()
            }

            binding.executePendingBindings()
        }

        fun deleteItem() {
            binding.deleteImage.setOnClickListener {
                onClickDeleteImage.invoke(binding.listItem!!.id.toString())
            }
        }

        fun onUrlSelected() {
            binding.labelTextView3.setOnClickListener {
                onUrlSelected.invoke(binding.listItem!!.url)
            }
        }
    }
}
