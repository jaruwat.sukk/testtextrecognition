package com.example.testtextrecognition.network

import com.google.gson.GsonBuilder
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class NetworkClient {

    inline fun <reified ApiService> createApi(okHttpClient: OkHttpClient, url: String): ApiService {
        val gsonBuilder = GsonBuilder().setLenient()
        val gson = gsonBuilder.create()
        val gsonConverterFactory = GsonConverterFactory.create(gson)
        val rxJava2CallAdapterFactory = RxJava2CallAdapterFactory.createWithScheduler(Schedulers.io())

        val retrofit = retrofit2.Retrofit.Builder()
            .baseUrl(url)
            .addConverterFactory(gsonConverterFactory)
            .addCallAdapterFactory(rxJava2CallAdapterFactory)
            .client(okHttpClient)
            .build()

        return retrofit.create(ApiService::class.java)
    }
}