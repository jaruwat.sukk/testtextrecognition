package com.example.testtextrecognition.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

class OkHttpClientFactory {

    companion object {

        private var instance: OkHttpClient? = null
        private var instanceMaxTimeout: OkHttpClient? = null
        const val MIN_CONNECTION_TIMEOUT = 20
        const val MAX_CONNECTION_TIMEOUT = 60

        fun getInstance(): OkHttpClient {
            if (instance == null) {
                instance = getInstance(MIN_CONNECTION_TIMEOUT)
            }
            return instance!!
        }

        fun getInstanceMaxTimeout(): OkHttpClient {
            if (instanceMaxTimeout == null) {
                instanceMaxTimeout = getInstance(MAX_CONNECTION_TIMEOUT)
            }
            return instanceMaxTimeout!!
        }

        private fun getInstance(connectTimeout: Int): OkHttpClient {
            val interceptor = HttpLoggingInterceptor()

            return OkHttpClient.Builder()
                    .connectTimeout(connectTimeout.toLong(), TimeUnit.SECONDS)
                    .readTimeout(connectTimeout.toLong(), TimeUnit.SECONDS)
                    .writeTimeout(connectTimeout.toLong(), TimeUnit.SECONDS)
                    .addInterceptor(interceptor)
                    .build()
        }

    }

}
