package com.example.testtextrecognition.network

import com.example.testtextrecognition.FoodRequest
import com.example.testtextrecognition.Model.FoodResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.POST

interface GetFoodListAPI {
    @POST(".")
    fun getList(@Body foodRequest : FoodRequest): Observable<FoodResponse>
}