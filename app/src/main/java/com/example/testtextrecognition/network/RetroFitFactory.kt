package com.example.testtextrecognition.network

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class RetroFitFactory {

    companion object {

        private var instance: Retrofit? = null

        fun getInstance(): Retrofit {
            if (instance == null) {
                instance = getInstance(
                        OkHttpClientFactory.getInstance()
                )
            }
            return instance!!
        }

        fun getInstance(client: OkHttpClient): Retrofit {

            return Retrofit.Builder()
                    .client(client)
                    .baseUrl(NetworkConstant.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
        }
    }
}