package com.example.testtextrecognition.Model

import com.example.testtextrecognition.FoodRequest
import io.reactivex.Observable

interface FoodDataSource {

    fun getListFood(foodRequest: FoodRequest): Observable<FoodResponse>

}