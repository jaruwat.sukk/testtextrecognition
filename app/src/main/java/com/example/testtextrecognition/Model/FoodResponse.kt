package com.example.testtextrecognition.Model

import FoodResult
import com.google.gson.annotations.SerializedName

data class FoodResponse(
    @SerializedName("content") val content: String = "",
    @SerializedName("num") val num: String = "",
    @SerializedName("output") val output: List<FoodResult> = listOf(),
    @SerializedName("re") val re: Any = Any(),
    @SerializedName("type") val type: Int = 0
)
