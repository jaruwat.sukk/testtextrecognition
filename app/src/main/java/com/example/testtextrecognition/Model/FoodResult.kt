import com.google.gson.annotations.SerializedName

data class FoodResult(
    @SerializedName("Addr") val addr: String = "",
    @SerializedName("cncnm") val cncnm: String = "",
    @SerializedName("IDA") val iDA: String = "",
    @SerializedName("lcnno") val lcnno: String = "",
    @SerializedName("licen") val licen: String = "",
    @SerializedName("NewCode") val newCode: String = "",
    @SerializedName("produceng") val produceng: String = "",
    @SerializedName("productha") val productha: String = "",
    @SerializedName("thanm") val thanm: String = "",
    @SerializedName("type") val type: String = "",
    @SerializedName("typeallow") val typeallow: String = "",
    @SerializedName("typepro") val typepro: String = "",
    @SerializedName("URLs") val uRLs: String = ""
)