package com.example.testtextrecognition.Model

import com.example.testtextrecognition.network.GetFoodListAPI
import com.example.testtextrecognition.network.RetroFitFactory

class FoodRepository private constructor(private val foodDataSource: FoodDataSource) : FoodDataSource by foodDataSource {

    companion object {
        fun newInstance(): FoodRepository {
            val api = RetroFitFactory.getInstance().create(GetFoodListAPI::class.java)
            return FoodRepository(FoodRemoteDataSource(api))
        }
    }

}