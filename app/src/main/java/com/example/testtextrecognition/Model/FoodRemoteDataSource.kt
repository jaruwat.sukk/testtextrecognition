package com.example.testtextrecognition.Model

import com.example.testtextrecognition.FoodRequest
import com.example.testtextrecognition.network.GetFoodListAPI
import io.reactivex.Observable

class FoodRemoteDataSource(private val getFoodListAPI: GetFoodListAPI) : FoodDataSource {

    override fun getListFood(foodRequest: FoodRequest): Observable<FoodResponse> {
        return getFoodListAPI.getList(foodRequest)
    }

}