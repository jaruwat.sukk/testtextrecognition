package com.example.testtextrecognition

import android.content.Intent
import android.database.sqlite.SQLiteDatabase
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_list_page.*


class ListPageActivity : AppCompatActivity() {

    private lateinit var mDb: SQLiteDatabase
    private lateinit var mHelper: MyDBClass

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_page)
        initView()
    }

    override fun onPause() {
        super.onPause()
        mHelper.close()
        mDb.close()
    }

    private fun initView() {
        mHelper = MyDBClass(this)
        mDb = mHelper.writableDatabase

        val listitemAdapter = MyRecyclerViewAdapter()
        listItemRecyclerView.apply {
            adapter = listitemAdapter

        }
        listitemAdapter.apply {
            updateSurveyStatus(getStatusNameList())
            onClickDeleteImage = {
                mHelper.DeleteData(it)
                updateSurveyStatus(getStatusNameList())
            }
            onUrlSelected = {
                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(it)))
            }
        }
    }

    private fun getStatusNameList(): List<ListItem> {
        return mHelper.SelectAllData() ?: listOf()
    }
}
