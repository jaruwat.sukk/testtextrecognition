package com.example.testtextrecognition

import com.google.gson.annotations.SerializedName

data class FoodRequest(
    @SerializedName("number_src") var number_src: String = "",
    @SerializedName("type") var type: Int = 0
)
